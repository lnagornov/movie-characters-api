using System.Net.Mime;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharactersWebApi.Models.Domain;
using MovieCharactersWebApi.Models.DTO.CharacterDTO;
using MovieCharactersWebApi.Repositories.Interfaces;

namespace MovieCharactersWebApi.Controllers;

[ApiController]
[Route("[controller]")]
[Produces(MediaTypeNames.Application.Json)]
[Consumes(MediaTypeNames.Application.Json)]
[ApiConventionType(typeof(DefaultApiConventions))]
public class CharactersController : ControllerBase
{
    private readonly ICharacterAsyncRepository _repository;
    private readonly IMapper _mapper;

    public CharactersController(ICharacterAsyncRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    // GET: /characters
    /// <summary>
    /// Get all characters
    /// </summary>
    /// <returns>List of all characters</returns>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<CharacterReadDto>>> GetCharacters()
    {
        var characters = await _repository.GetAllAsync();
        var charactersReadDto = _mapper.Map<List<CharacterReadDto>>(characters);

        return Ok(charactersReadDto);
    }

    // GET: /characters/1
    /// <summary>
    /// Get one specific character by character id
    /// </summary>
    /// <param name="id">Id of the character to get</param>
    /// <returns>One specific characters or Status code 404 Not Found (failure)</returns>
    [HttpGet("{id:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<CharacterCreateDto>> GetCharacter(int id)
    {
        var character = await _repository.GetByIdAsync(id);
        if (character is null)
        {
            return NotFound();
        }

        var characterReadDto = _mapper.Map<CharacterReadDto>(character);

        return Ok(characterReadDto);
    }

    // PUT: /characters/1
    /// <summary>
    /// Update a character by character id
    /// </summary>
    /// <param name="id">Id of the character to update</param>
    /// <param name="characterEditDto">Character Edit DTO model to update on</param>
    /// <returns>Status code 204 No content (success) or Status code 404 Not found (failure)</returns>
    [HttpPut("{id:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> PutCharacter(int id, CharacterEditDto characterEditDto)
    {
        if (id != characterEditDto.Id)
        {
            return BadRequest();
        }

        if (!await _repository.ExistsWithIdAsync(id))
        {
            return NotFound();
        }

        var character = _mapper.Map<Character>(characterEditDto);
        await _repository.UpdateAsync(character);

        return NoContent();
    }

    // POST: /characters
    /// <summary>
    /// Add a new character
    /// </summary>
    /// <param name="characterCreateDto">Character Create DTO model to add new Character</param>
    /// <returns>Status code 204 No content (success) or Status code 404 Not found (failure)</returns>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    public async Task<ActionResult<CharacterReadDto>> PostCharacter(CharacterCreateDto characterCreateDto)
    {
        var character = _mapper.Map<Character>(characterCreateDto);
        character = await _repository.AddAsync(character);

        return CreatedAtAction(
                    nameof(GetCharacter),
                    new {id = character!.Id},
                    _mapper.Map<CharacterReadDto>(character));
    }

    // DELETE: /characters/1
    /// <summary>
    /// Delete a character by character id
    /// </summary>
    /// <param name="id">Id of the character to delete</param>
    /// <returns>Status code 204 No content (success) or Status code 404 Not found (failure)</returns>
    [HttpDelete("{id:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> DeleteCharacter(int id)
    {
        if (!await _repository.ExistsWithIdAsync(id))
        {
            return NotFound();
        }

        await _repository.DeleteByIdAsync(id);

        return NoContent();
    }
}