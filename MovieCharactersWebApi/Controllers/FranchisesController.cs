using System.Net.Mime;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharactersWebApi.Models.Domain;
using MovieCharactersWebApi.Models.DTO.CharacterDTO;
using MovieCharactersWebApi.Models.DTO.FranchiseDTO;
using MovieCharactersWebApi.Models.DTO.MovieDTO;
using MovieCharactersWebApi.Repositories.Interfaces;


namespace MovieCharactersWebApi.Controllers;

[ApiController]
[Route("[controller]")]
[Produces(MediaTypeNames.Application.Json)]
[Consumes(MediaTypeNames.Application.Json)]
[ApiConventionType(typeof(DefaultApiConventions))]
public class FranchisesController: ControllerBase
{
    private readonly IFranchiseAsyncRepository _repository;
    private readonly IMapper _mapper;

    public FranchisesController(IFranchiseAsyncRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    // GET: /franchises
    /// <summary>
    /// Get all franchises
    /// </summary>
    /// <returns>List of all franchises</returns>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<FranchiseReadDto>>> GetFranchises()
    {
        var franchises = await _repository.GetAllAsync();
        var franchisesReadDto = _mapper.Map<List<FranchiseReadDto>>(franchises);

        return Ok(franchisesReadDto);
    }

    // GET: /franchises/1
    /// <summary>
    /// Get one specific franchise by franchise id
    /// </summary>
    /// <param name="id">Id of the franchise to get</param>
    /// <returns>One specific franchise or Status code 404 Not Found (failure)</returns>
    [HttpGet("{id:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<FranchiseCreateDto>> GetFranchise(int id)
    {
        var franchise = await _repository.GetByIdAsync(id);
        if (franchise is null)
        {
            return NotFound();
        }

        var franchiseReadDto = _mapper.Map<FranchiseReadDto>(franchise);

        return Ok(franchiseReadDto);
    }

    // PUT: /franchises/1
    /// <summary>
    /// Update a franchise by franchise id
    /// </summary>
    /// <param name="id">Id of the franchise to update</param>
    /// <param name="franchiseEditDto">Franchise Edit DTO model to update on</param>
    /// <returns>Status code 204 No content (success) or Status code 404 Not found (failure)</returns>
    [HttpPut("{id:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> PutFranchise(int id, FranchiseEditDto franchiseEditDto)
    {
        if (id != franchiseEditDto.Id)
        {
            return BadRequest();
        }

        if (!await _repository.ExistsWithIdAsync(id))
        {
            return NotFound();
        }

        var franchise = _mapper.Map<Franchise>(franchiseEditDto);
        await _repository.UpdateAsync(franchise);

        return NoContent();
    }

    // POST: /franchises
    /// <summary>
    /// Add a new franchise
    /// </summary>
    /// <param name="franchiseCreateDto">Franchise Create DTO model to add new Franchise</param>
    /// <returns>Status code 204 No content (success) or Status code 404 Not found (failure)</returns>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    public async Task<ActionResult<FranchiseReadDto>> PostFranchise(FranchiseCreateDto franchiseCreateDto)
    {
        var franchise = _mapper.Map<Franchise>(franchiseCreateDto);
        franchise = await _repository.AddAsync(franchise);

        return CreatedAtAction(
                    nameof(GetFranchise),
                    new {id = franchise!.Id},
                    _mapper.Map<FranchiseReadDto>(franchise));
    }

    // DELETE: /franchises/1
    /// <summary>
    /// Delete a franchise by franchise id
    /// </summary>
    /// <param name="id">Id of the franchise to delete</param>
    /// <returns>Status code 204 No content (success) or Status code 404 Not found (failure)</returns>
    [HttpDelete("{id:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> DeleteFranchise(int id)
    {
        if (!await _repository.ExistsWithIdAsync(id))
        {
            return NotFound();
        }

        await _repository.DeleteByIdAsync(id);

        return NoContent();
    }
    
    // GET: /franchises/1/movies
    /// <summary>
    /// Get all movies from a franchise by franchise id
    /// </summary>
    /// <param name="id">Id of the franchise to get movies from</param>
    /// <returns>List of movies of the franchise or Status code 404 Not Found (failure)</returns>
    [HttpGet("{id:int}/Movies")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<MovieReadDto>> GetFranchiseMovies(int id)
    {
        if (!await _repository.ExistsWithIdAsync(id))
            return NotFound();

        var franchiseMovies = await _repository.GetMoviesByFranchiseIdAsync(id);
        var moviesReadDto = _mapper.Map<List<MovieReadDto>>(franchiseMovies);

        return Ok(moviesReadDto);
    }
    
    // GET: /franchises/1/characters
    /// <summary>
    /// Get all characters from a franchise by franchise id
    /// </summary>
    /// <param name="id">Id of the franchise to get characters from</param>
    /// <returns>List of characters of the franchise or Status code 404 Not Found (failure)</returns>
    [HttpGet("{id:int}/Characters")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<CharacterReadDto>> GetFranchiseCharacters(int id)
    {
        if (!await _repository.ExistsWithIdAsync(id))
            return NotFound();

        var franchiseCharacters = await _repository.GetCharactersByFranchiseIdAsync(id);
        var charactersReadDto = _mapper.Map<List<CharacterReadDto>>(franchiseCharacters);

        return Ok(charactersReadDto);
    }
    
    // PUT: /franchises/1/movies
    /// <summary>
    /// Update all movies from a franchise by franchise id with enumerable of movies ids
    /// </summary>
    /// <param name="id">Id of the franchise to update characters</param>
    /// <param name="movieIds">Enumerable of movies ids to replace</param>
    /// <returns>Status code 204 No content (success) or Status code 404 Not found (failure)</returns>
    [HttpPut("{id:int}/Movies")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> PutFranchiseMovies(int id, IEnumerable<int> movieIds)
    {
        if (!await _repository.ExistsWithIdAsync(id))
        {
            return NotFound();
        }

        try
        {
            await _repository.UpdateFranchiseMoviesByFranchiseIdAsync(id, movieIds);
        }
        catch (KeyNotFoundException e)
        {
            return BadRequest(e.Message);
        }

        return NoContent();
    }
}