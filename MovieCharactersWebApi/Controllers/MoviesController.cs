using System.Net.Mime;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharactersWebApi.Models.Domain;
using MovieCharactersWebApi.Models.DTO.CharacterDTO;
using MovieCharactersWebApi.Models.DTO.MovieDTO;
using MovieCharactersWebApi.Repositories.Interfaces;

namespace MovieCharactersWebApi.Controllers;

[ApiController]
[Route("[controller]")]
[Produces(MediaTypeNames.Application.Json)]
[Consumes(MediaTypeNames.Application.Json)]
[ApiConventionType(typeof(DefaultApiConventions))]
public class MoviesController : ControllerBase
{
    private readonly IMovieAsyncRepository _repository;
    private readonly IMapper _mapper;

    public MoviesController(IMovieAsyncRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    // GET: /movies
    /// <summary>
    /// Get all movies
    /// </summary>
    /// <returns>List of all movies</returns>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<MovieReadDto>>> GetMovies()
    {
        var movies = await _repository.GetAllAsync();
        var moviesReadDto = _mapper.Map<List<MovieReadDto>>(movies);

        return Ok(moviesReadDto);
    }

    // GET: /movies/1
    /// <summary>
    /// Get one specific movie by movie id
    /// </summary>
    /// <param name="id">Id of the movie to get</param>
    /// <returns>One specific movie or Status code 404 Not Found (failure)</returns>
    [HttpGet("{id:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<MovieReadDto>> GetMovie(int id)
    {
        var movie = await _repository.GetByIdAsync(id);
        if (movie is null)
        {
            return NotFound();
        }

        var movieReadDto = _mapper.Map<MovieReadDto>(movie);

        return Ok(movieReadDto);
    }

    // PUT: /movies/1
    /// <summary>
    /// Update a movie by movie id
    /// </summary>
    /// <param name="id">Id of the movie to update</param>
    /// <param name="movieEditDto">Movie Edit DTO model to update on</param>
    /// <returns>Status code 204 No content (success) or Status code 404 Not found (failure)</returns>
    [HttpPut("{id:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> PutMovie(int id, MovieEditDto movieEditDto)
    {
        if (id != movieEditDto.Id)
        {
            return BadRequest();
        }

        if (!await _repository.ExistsWithIdAsync(id))
        {
            return NotFound();
        }

        var movie = _mapper.Map<Movie>(movieEditDto);
        await _repository.UpdateAsync(movie);

        return NoContent();
    }

    // POST: /movies
    /// <summary>
    /// Add a new movie
    /// </summary>
    /// <param name="movieCreateDto">Movie Create DTO model to add new Movie</param>
    /// <returns>Status code 204 No content (success) or Status code 404 Not found (failure)</returns>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    public async Task<ActionResult<MovieReadDto>> PostMovie(MovieCreateDto movieCreateDto)
    {
        var movie = _mapper.Map<Movie>(movieCreateDto);
        movie = await _repository.AddAsync(movie);

        return CreatedAtAction(
                    nameof(GetMovie),
                    new {id = movie!.Id},
                    _mapper.Map<MovieReadDto>(movie));
    }

    // DELETE: /movies/1
    /// <summary>
    /// Delete a movie by movie id
    /// </summary>
    /// <param name="id">Id of the movie to delete</param>
    /// <returns>Status code 204 No content (success) or Status code 404 Not found (failure)</returns>
    [HttpDelete("{id:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> DeleteMovie(int id)
    {
        if (!await _repository.ExistsWithIdAsync(id))
        {
            return NotFound();
        }

        await _repository.DeleteByIdAsync(id);

        return NoContent();
    }

    // GET: /movies/1/characters
    /// <summary>
    /// Get all characters from a movie by movie id
    /// </summary>
    /// <param name="id">Id of the movie to get characters</param>
    /// <returns>List of characters of the movie or Status code 404 Not Found (failure)</returns>
    [HttpGet("{id:int}/Characters")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<CharacterReadDto>> GetMovieCharacters(int id)
    {
        if (!await _repository.ExistsWithIdAsync(id))
            return NotFound();

        var movieCharacters = await _repository.GetCharactersByMovieIdAsync(id);
        var charactersReadDto = _mapper.Map<List<CharacterReadDto>>(movieCharacters);

        return Ok(charactersReadDto);
    }
    
    // PUT: /movies/1/characters
    /// <summary>
    /// Update all characters from a movie by movie id with enumerable of character ids
    /// </summary>
    /// <param name="id">Id of the movie to update characters</param>
    /// <param name="characterIds">Enumerable of character ids to replace</param>
    /// <returns>Status code 204 No content (success) or Status code 404 Not found (failure)</returns>
    [HttpPut("{id:int}/Characters")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> PutMovieCharacters(int id, IEnumerable<int> characterIds)
    {
        if (!await _repository.ExistsWithIdAsync(id))
        {
            return NotFound();
        }

        try
        {
            await _repository.UpdateMovieCharactersByMovieIdAsync(id, characterIds);
        }
        catch (KeyNotFoundException e)
        {
            return BadRequest(e.Message);
        }

        return NoContent();
    }
}