﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace MovieCharactersWebApi.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Character",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FullName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Alias = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Gender = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Picture = table.Column<string>(type: "character varying(2048)", maxLength: 2048, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Character", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchise",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "character varying(5000)", maxLength: 5000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchise", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movie",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ReleaseYear = table.Column<int>(type: "integer", nullable: true),
                    Title = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "character varying(5000)", maxLength: 5000, nullable: true),
                    Genre = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Director = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Picture = table.Column<string>(type: "character varying(2048)", maxLength: 2048, nullable: true),
                    Trailer = table.Column<string>(type: "character varying(2048)", maxLength: 2048, nullable: true),
                    FranchiseId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movie_Franchise_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchise",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacter",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "integer", nullable: false),
                    CharacterId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacter", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Character_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Character",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Movie_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, "Mr. Baggins", "Bilbo Baggins", "Male", "https://upload.wikimedia.org/wikipedia/en/2/2f/Young_Bilbo.jpg" },
                    { 2, "Frodo of the Nine Fingers", "Frodo Baggins", "Male", "https://upload.wikimedia.org/wikipedia/en/4/4e/Elijah_Wood_as_Frodo_Baggins.png" },
                    { 3, "Mithrandir", "Gandalf", "Male", "https://upload.wikimedia.org/wikipedia/en/e/e9/Gandalf600ppx.jpg" },
                    { 4, "Strider", "Aragorn II Elessar", "Male", "https://upload.wikimedia.org/wikipedia/en/3/35/Aragorn300ppx.png" },
                    { 5, "Lady of Light", "Galadriel", "Female", "https://upload.wikimedia.org/wikipedia/en/c/cb/Galadriel.jpg" },
                    { 6, "Smaug the Terrible", "Smaug", "Male", "https://upload.wikimedia.org/wikipedia/en/f/f2/Smaug_%28Peter_Jackson_Hobbit_Trilogy%29.jpg" },
                    { 7, "King of Durin's Folk", "Thorin II 'Oakenshield'", "Male", "https://upload.wikimedia.org/wikipedia/en/e/ed/Thorin%2C_from_the_Hobbit.jpg" },
                    { 8, "Dumbledore", "Albus Percival Wulfric Brian Dumbledore", "Male", "https://upload.wikimedia.org/wikipedia/en/f/fe/Dumbledore_and_Elder_Wand.JPG" },
                    { 9, "The boy who lived", "Harry James Potter", "Male", "https://upload.wikimedia.org/wikipedia/en/d/d7/Harry_Potter_character_poster.jpg" },
                    { 10, "Newt", "Newton Artemis Fido Scamander", "Male", "https://static.wikia.nocookie.net/harrypotter/images/3/36/Newton_Scamander_Profile_crop.png/revision/latest?cb=20190609204955" }
                });

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Middle-earth is the fictional setting of much of the English writer J. R. R. Tolkien's fantasy. New Line Cinema released the first part of director Peter Jackson's The Lord of the Rings film series in 2001 as part of a trilogy and several actors and roles were introduced once again in a trilogy in The Hobbit film series. In 2017, Amazon co-operated with New Line to acquire the television rights to adapt a new prequel show set in a period glimpsed during a flashback in The Lord of the Rings films.", "Middle-earth" },
                    { 2, "The Wizarding World (previously known as J. K. Rowling's Wizarding World) is a fantasy media franchise and shared fictional universe centred on the Harry Potter novel series by J. K. Rowling. A series of films have been in production since 2000, and in that time eleven films have been produced—eight are adaptations of the Harry Potter novels and three are part of the Fantastic Beasts series.", "Wizarding World" }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Description", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 1, "A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron.", "Peter Jackson", 1, "Fantasy, Action, Adventure, Drama", "https://upload.wikimedia.org/wikipedia/en/8/8a/The_Lord_of_the_Rings_The_Fellowship_of_the_Ring_%282001%29.jpg", 2001, "The Lord of the Rings: The Fellowship of the Ring", "https://www.youtube.com/watch?v=V75dMMIW2B4" },
                    { 2, "While Frodo and Sam edge closer to Mordor with the help of the shifty Gollum, the divided fellowship makes a stand against Sauron's new ally, Saruman, and his hordes of Isengard.", "Peter Jackson", 1, "Fantasy, Action, Adventure, Drama", "https://upload.wikimedia.org/wikipedia/en/d/d0/Lord_of_the_Rings_-_The_Two_Towers_%282002%29.jpg", 2002, "The Lord of the Rings: The Two Towers", "https://www.youtube.com/watch?v=r5X-hFf6Bwo" },
                    { 3, "Gandalf and Aragorn lead the World of Men against Sauron's army to draw his gaze from Frodo and Sam as they approach Mount Doom with the One Ring.", "Peter Jackson", 1, "Fantasy, Action, Adventure, Drama", "https://upload.wikimedia.org/wikipedia/en/b/be/The_Lord_of_the_Rings_-_The_Return_of_the_King_%282003%29.jpg", 2003, "The Lord of the Rings: The Return of the King", "https://www.youtube.com/watch?v=r5X-hFf6Bwo" },
                    { 4, "A reluctant Hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home, and the gold within it from the dragon Smaug.", "Peter Jackson", 1, "Fantasy, Adventure", "https://upload.wikimedia.org/wikipedia/en/b/b3/The_Hobbit-_An_Unexpected_Journey.jpeg", 2012, "The Hobbit: An Unexpected Journey", "https://www.youtube.com/watch?v=9PSXjr1gbjc" },
                    { 5, "The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring.", "Peter Jackson", 1, "Fantasy, Adventure", "https://upload.wikimedia.org/wikipedia/en/4/4f/The_Hobbit_-_The_Desolation_of_Smaug_theatrical_poster.jpg", 2013, "The Hobbit: The Desolation of Smaug", "https://www.youtube.com/watch?v=fnaojlfdUbs" },
                    { 6, "Bilbo and company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness.", "Peter Jackson", 1, "Fantasy, Adventure", "https://upload.wikimedia.org/wikipedia/en/0/0e/The_Hobbit_-_The_Battle_of_the_Five_Armies.jpg", 2014, "The Hobbit: The Battle of the Five Armies", "https://www.youtube.com/watch?v=ZSzeFFsKEt4" },
                    { 7, "An orphaned boy enrolls in a school of wizardry, where he learns the truth about himself, his family and the terrible evil that haunts the magical world.", "Chris Columbus", 2, "Fantasy, Adventure, Family", "https://upload.wikimedia.org/wikipedia/en/7/7a/Harry_Potter_and_the_Philosopher%27s_Stone_banner.jpg", 2001, "Harry Potter and the Philosopher's Stone", "https://www.youtube.com/watch?v=VyHV0BRtdxo" },
                    { 8, "An ancient prophecy seems to be coming true when a mysterious presence begins stalking the corridors of a school of magic and leaving its victims paralyzed.", "Chris Columbus", 2, "Fantasy, Adventure, Family", "https://upload.wikimedia.org/wikipedia/en/c/c0/Harry_Potter_and_the_Chamber_of_Secrets_movie.jpg", 2002, "Harry Potter and the Chamber of Secrets", "https://www.youtube.com/watch?v=1bq0qff4iF8" },
                    { 9, "Harry Potter, Ron and Hermione return to Hogwarts School of Witchcraft and Wizardry for their third year of study, where they delve into the mystery surrounding an escaped prisoner who poses a dangerous threat to the young wizard.", "Alfonso Cuarón", 2, "Fantasy, Adventure, Family", "https://upload.wikimedia.org/wikipedia/en/b/bc/Prisoner_of_azkaban_UK_poster.jpg", 2004, "Harry Potter and the Prisoner of Azkaban", "https://www.youtube.com/watch?v=1ZdlAg3j8nI" },
                    { 10, "Harry Potter finds himself competing in a hazardous tournament between rival schools of magic, but he is distracted by recurring nightmares.", "Mike Newell", 2, "Fantasy, Adventure, Family", "https://upload.wikimedia.org/wikipedia/en/c/c9/Harry_Potter_and_the_Goblet_of_Fire_Poster.jpg", 2005, "Harry Potter and the Goblet of Fire", "https://www.youtube.com/watch?v=3EGojp4Hh6I" },
                    { 11, "With their warning about Lord Voldemort's return scoffed at, Harry and Dumbledore are targeted by the Wizard authorities as an authoritarian bureaucrat slowly seizes power at Hogwarts.", "David Yates", 2, "Fantasy, Adventure, Family, Action", "https://upload.wikimedia.org/wikipedia/en/e/e7/Harry_Potter_and_the_Order_of_the_Phoenix_poster.jpg", 2007, "Harry Potter and the Order of the Phoenix", "https://www.youtube.com/watch?v=y6ZW7KXaXYk" },
                    { 12, "As Harry Potter begins his sixth year at Hogwarts, he discovers an old book marked as 'the property of the Half-Blood Prince' and begins to learn more about Lord Voldemort's dark past.", "David Yates", 2, "Fantasy, Adventure, Family, Action", "https://upload.wikimedia.org/wikipedia/en/3/3f/Harry_Potter_and_the_Half-Blood_Prince_poster.jpg", 2009, "Harry Potter and the Half-Blood Prince", "https://www.youtube.com/watch?v=sg81Lts5kYY" },
                    { 13, "As Harry, Ron, and Hermione race against time and evil to destroy the Horcruxes, they uncover the existence of the three most powerful objects in the wizarding world: the Deathly Hallows.", "David Yates", 2, "Fantasy, Adventure, Family", "https://upload.wikimedia.org/wikipedia/en/2/2d/Harry_Potter_and_the_Deathly_Hallows_–_Part_1.jpg", 2011, "Harry Potter and the Deathly Hallows: Part 1", "https://www.youtube.com/watch?v=MxqsmsA8y5k" },
                    { 14, "Harry, Ron, and Hermione search for Voldemort's remaining Horcruxes in their effort to destroy the Dark Lord as the final battle rages on at Hogwarts.", "David Yates", 2, "Fantasy, Adventure, Family, Mystery", "https://upload.wikimedia.org/wikipedia/en/d/df/Harry_Potter_and_the_Deathly_Hallows_–_Part_2.jpg", 2012, "Harry Potter and the Deathly Hallows: Part 2", "https://www.youtube.com/watch?v=PUXpgitNeOU" },
                    { 15, "The adventures of writer Newt Scamander in New York's secret community of witches and wizards seventy years before Harry Potter reads his book in school.", "David Yates", 2, "Fantasy, Adventure, Family", "https://upload.wikimedia.org/wikipedia/en/5/5e/Fantastic_Beasts_and_Where_to_Find_Them_poster.png", 2016, "Fantastic Beasts and Where to Find Them", "https://www.youtube.com/watch?v=ViuDsy7yb8M" },
                    { 16, "The second installment of the 'Fantastic Beasts' series featuring the adventures of Magizoologist Newt Scamander.", "David Yates", 2, "Fantasy, Adventure, Family", "https://upload.wikimedia.org/wikipedia/en/3/3c/Fantastic_Beasts_-_The_Crimes_of_Grindelwald_Poster.png", 2018, "Fantastic Beasts: The Crimes of Grindelwald", "https://www.youtube.com/watch?v=5sEaYB4rLFQ" },
                    { 17, "Albus Dumbledore assigns Newt and his allies with a mission related to the rising power of Grindelwald.", "David Yates", 2, "Fantasy, Action, Adventure", "https://upload.wikimedia.org/wikipedia/en/3/34/Fantastic_Beasts-_The_Secrets_of_Dumbledore.png", 2022, "Fantastic Beasts: The Secrets of Dumbledore", "https://www.youtube.com/watch?v=Y9dr2zw-TXQ" }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacter",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 1 },
                    { 4, 1 },
                    { 5, 1 },
                    { 2, 2 },
                    { 3, 2 },
                    { 4, 2 },
                    { 5, 2 },
                    { 1, 3 },
                    { 2, 3 },
                    { 3, 3 },
                    { 4, 3 },
                    { 5, 3 },
                    { 1, 4 },
                    { 2, 4 },
                    { 3, 4 },
                    { 5, 4 },
                    { 6, 4 },
                    { 7, 4 },
                    { 1, 5 },
                    { 3, 5 },
                    { 5, 5 },
                    { 6, 5 },
                    { 7, 5 },
                    { 1, 6 },
                    { 3, 6 },
                    { 5, 6 },
                    { 7, 6 },
                    { 8, 7 },
                    { 9, 7 },
                    { 8, 8 },
                    { 9, 8 },
                    { 8, 9 },
                    { 9, 9 },
                    { 8, 10 },
                    { 9, 10 },
                    { 8, 11 },
                    { 9, 11 },
                    { 8, 12 },
                    { 9, 12 },
                    { 8, 13 },
                    { 9, 13 },
                    { 8, 14 },
                    { 9, 14 },
                    { 8, 15 },
                    { 10, 15 },
                    { 8, 16 },
                    { 10, 16 },
                    { 8, 17 },
                    { 10, 17 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Movie_FranchiseId",
                table: "Movie",
                column: "FranchiseId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacter_CharacterId",
                table: "MovieCharacter",
                column: "CharacterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacter");

            migrationBuilder.DropTable(
                name: "Character");

            migrationBuilder.DropTable(
                name: "Movie");

            migrationBuilder.DropTable(
                name: "Franchise");
        }
    }
}
