namespace MovieCharactersWebApi.Models.DTO.CharacterDTO;

public class CharacterCreateDto
{
    public string? FullName { get; set; }
    public string? Alias { get; set; }
    public string? Gender { get; set; }
    public string? Picture { get; set; }
}