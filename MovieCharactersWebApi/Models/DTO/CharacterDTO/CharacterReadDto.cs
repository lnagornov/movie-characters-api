using MovieCharactersWebApi.Models.DTO.MovieDTO;

namespace MovieCharactersWebApi.Models.DTO.CharacterDTO;

public class CharacterReadDto
{
    public int Id { get; set; }
    public string? FullName { get; set; }
    public string? Alias { get; set; }
    public string? Gender { get; set; }
    public string? Picture { get; set; }
    public List<MovieWithOutCharactersReadDto?>? Movies { get; set; }
}