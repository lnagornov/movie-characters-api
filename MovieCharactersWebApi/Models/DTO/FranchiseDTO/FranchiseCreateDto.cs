namespace MovieCharactersWebApi.Models.DTO.FranchiseDTO;

public class FranchiseCreateDto
{
    public string? Name { get; set; }
    public string? Description { get; set; }
}