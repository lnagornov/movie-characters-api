namespace MovieCharactersWebApi.Models.DTO.FranchiseDTO;

public class FranchiseEditDto
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? Description { get; set; }
}