using MovieCharactersWebApi.Models.DTO.CharacterDTO;

namespace MovieCharactersWebApi.Models.DTO.MovieDTO;

public class MovieReadDto
{
    public int Id { get; set; }
    public int? ReleaseYear { get; set; }
    public string? Title { get; set; }
    public string? Description { get; set; }
    public string? Genre { get; set; }
    public string? Director { get; set; }
    public string? Picture { get; set; }
    public string? Trailer { get; set; }
    public int? FranchiseId { get; set; }
    public List<CharacterWithOutMoviesReadDto?>? Characters { get; set; }
}