using System.ComponentModel.DataAnnotations;

namespace MovieCharactersWebApi.Models.Domain;

public class Character
{
    public int Id { get; set; }
    
    [Required]
    [MaxLength(255)]
    public string? FullName { get; set; }
    
    [MaxLength(255)]
    public string? Alias { get; set; }
    
    [MaxLength(255)]
    public string? Gender { get; set; }
    
    [MaxLength(2048)]
    public string? Picture { get; set; }
    
    // Navigation properties
    // The relationship between Character and Movie is many-to-many
    // A character can play in multiple movies and one movie could contain many characters
    public ICollection<Movie?> Movies { get; set; }
}