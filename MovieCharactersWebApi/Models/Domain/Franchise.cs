using System.ComponentModel.DataAnnotations;

namespace MovieCharactersWebApi.Models.Domain;

public class Franchise
{
    public int Id { get; set; }
    
    [Required]
    [MaxLength(255)]
    public string? Name { get; set; }
    
    [MaxLength(5000)]
    public string? Description { get; set; }
    
    // Navigation properties
    // The relationship between Franchise and Movie is one-to-many
    // A franchise can contain many movies, but one movie belongs to only one franchise
    public ICollection<Movie?> Movies { get; set; }
}