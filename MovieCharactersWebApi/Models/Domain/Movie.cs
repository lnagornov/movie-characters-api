using System.ComponentModel.DataAnnotations;

namespace MovieCharactersWebApi.Models.Domain;

public class Movie
{
    public int Id { get; set; }
    public int? ReleaseYear { get; set; }
    
    [Required]
    [MaxLength(255)]
    public string? Title { get; set; }

    [MaxLength(5000)]
    public string? Description { get; set; }
    
    [MaxLength(255)]
    public string? Genre { get; set; }

    [MaxLength(255)]
    public string? Director { get; set; }
    
    [MaxLength(2048)]
    public string? Picture { get; set; }
    
    [MaxLength(2048)]
    public string? Trailer { get; set; }
    
    // Navigation properties
    // The relationship between Franchise and Movie is one-to-many
    // A franchise can contain many movies, but one movie belongs to only one franchise
    public int? FranchiseId { get; set; }
    public Franchise? Franchise { get; set; }
    
    // The relationship between Character and Movie is many-to-many
    // A character can play in multiple movies and one movie could contain many characters
    public ICollection<Character?> Characters { get; set; }
    
}   