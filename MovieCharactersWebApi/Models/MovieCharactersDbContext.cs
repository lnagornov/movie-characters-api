using Microsoft.EntityFrameworkCore;
using MovieCharactersWebApi.Models.Domain;

namespace MovieCharactersWebApi.Models;

public class MovieCharactersDbContext : DbContext
{
    // Create tables
    public DbSet<Movie>? Movie { get; set; }
    public DbSet<Character>? Character { get; set; }
    public DbSet<Franchise>? Franchise { get; set; }

    public MovieCharactersDbContext(DbContextOptions options) : base(options)
    {
    }

    // Initial filling data
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Define one-to-many relationships between Franchise and Movie
        // Set null on delete
        modelBuilder.Entity<Movie>()
            .HasOne(m => m.Franchise)
            .WithMany(f => f.Movies)
            .HasForeignKey(m => m.FranchiseId)
            .OnDelete(DeleteBehavior.SetNull);
        
        // Initialize tables with seed data
        modelBuilder.Entity<Movie>().HasData(SeedHelper.GetMovies());
        modelBuilder.Entity<Character>().HasData(SeedHelper.GetCharacters());
        modelBuilder.Entity<Franchise>().HasData(SeedHelper.GetFranchises());

        // Define many-to-many map table and insert data
        modelBuilder.Entity<Movie>()
            .HasMany(movie => movie.Characters)
            .WithMany(character => character.Movies)
            .UsingEntity<Dictionary<string, object>>(
                "MovieCharacter",
                left => left.HasOne<Character>().WithMany().HasForeignKey("CharacterId")!,
                right => right.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                joinEntity =>
                {
                    joinEntity.HasKey("MovieId", "CharacterId");
                    joinEntity.HasData(
                        // Bilbo
                        new {MovieId = 1, CharacterId = 1},
                        new {MovieId = 3, CharacterId = 1},
                        new {MovieId = 4, CharacterId = 1},
                        new {MovieId = 5, CharacterId = 1},
                        new {MovieId = 6, CharacterId = 1},
                        
                        // Frodo
                        new {MovieId = 1, CharacterId = 2},
                        new {MovieId = 2, CharacterId = 2},
                        new {MovieId = 3, CharacterId = 2},
                        new {MovieId = 4, CharacterId = 2},

                        // Gandalf
                        new {MovieId = 1, CharacterId = 3},
                        new {MovieId = 2, CharacterId = 3},
                        new {MovieId = 3, CharacterId = 3},
                        new {MovieId = 4, CharacterId = 3},
                        new {MovieId = 5, CharacterId = 3},
                        new {MovieId = 6, CharacterId = 3},
                        
                        // Aragorn
                        new {MovieId = 1, CharacterId = 4},
                        new {MovieId = 2, CharacterId = 4},
                        new {MovieId = 3, CharacterId = 4},
                        
                        // Galadriel
                        new {MovieId = 1, CharacterId = 5},
                        new {MovieId = 2, CharacterId = 5},
                        new {MovieId = 3, CharacterId = 5},
                        new {MovieId = 4, CharacterId = 5},
                        new {MovieId = 5, CharacterId = 5},
                        new {MovieId = 6, CharacterId = 5},
                        
                        // Smaug
                        new {MovieId = 4, CharacterId = 6},
                        new {MovieId = 5, CharacterId = 6},

                        // Thorin
                        new {MovieId = 4, CharacterId = 7},
                        new {MovieId = 5, CharacterId = 7},
                        new {MovieId = 6, CharacterId = 7},
                        
                        // Albus Dumbledore
                        new {MovieId = 7, CharacterId = 8},
                        new {MovieId = 8, CharacterId = 8},
                        new {MovieId = 9, CharacterId = 8},
                        new {MovieId = 10, CharacterId = 8},
                        new {MovieId = 11, CharacterId = 8},
                        new {MovieId = 12, CharacterId = 8},
                        new {MovieId = 13, CharacterId = 8},
                        new {MovieId = 14, CharacterId = 8},
                        new {MovieId = 15, CharacterId = 8},
                        new {MovieId = 16, CharacterId = 8},
                        new {MovieId = 17, CharacterId = 8},

                        // Harry Potter
                        new {MovieId = 7, CharacterId = 9},
                        new {MovieId = 8, CharacterId = 9},
                        new {MovieId = 9, CharacterId = 9},
                        new {MovieId = 10, CharacterId = 9},
                        new {MovieId = 11, CharacterId = 9},
                        new {MovieId = 12, CharacterId = 9},
                        new {MovieId = 13, CharacterId = 9},
                        new {MovieId = 14, CharacterId = 9},
                        
                        // Newt Scamander
                        new {MovieId = 15, CharacterId = 10},
                        new {MovieId = 16, CharacterId = 10},
                        new {MovieId = 17, CharacterId = 10}
                    );
                }
            );
    }
}