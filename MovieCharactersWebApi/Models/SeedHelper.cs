using MovieCharactersWebApi.Models.Domain;

namespace MovieCharactersWebApi.Models;

public static class SeedHelper
{
    /// <summary>
    /// Creates a list of movies to initialize database
    /// </summary>
    /// <returns>List of movies</returns>
    public static IEnumerable<Movie> GetMovies()
    {
        return new List<Movie>()
        {
            // The Lord of the Rings trilogy
            new()
            {
                Id = 1, 
                Director = "Peter Jackson", 
                Genre = "Fantasy, Action, Adventure, Drama", 
                Picture = "https://upload.wikimedia.org/wikipedia/en/8/8a/The_Lord_of_the_Rings_The_Fellowship_of_the_Ring_%282001%29.jpg", 
                Title = "The Lord of the Rings: The Fellowship of the Ring", 
                Trailer = "https://www.youtube.com/watch?v=V75dMMIW2B4", 
                ReleaseYear = 2001,
                Description = "A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron.",
                FranchiseId = 1
            },
            new()
            {
                Id = 2, 
                Director = "Peter Jackson", 
                Genre = "Fantasy, Action, Adventure, Drama", 
                Picture = "https://upload.wikimedia.org/wikipedia/en/d/d0/Lord_of_the_Rings_-_The_Two_Towers_%282002%29.jpg", 
                Title = "The Lord of the Rings: The Two Towers", 
                Trailer = "https://www.youtube.com/watch?v=r5X-hFf6Bwo", 
                ReleaseYear = 2002,
                Description = "While Frodo and Sam edge closer to Mordor with the help of the shifty Gollum, the divided fellowship makes a stand against Sauron's new ally, Saruman, and his hordes of Isengard.",
                FranchiseId = 1
            },
            new()
            {
                Id = 3, 
                Director = "Peter Jackson", 
                Genre = "Fantasy, Action, Adventure, Drama", 
                Picture = "https://upload.wikimedia.org/wikipedia/en/b/be/The_Lord_of_the_Rings_-_The_Return_of_the_King_%282003%29.jpg", 
                Title = "The Lord of the Rings: The Return of the King", 
                Trailer = "https://www.youtube.com/watch?v=r5X-hFf6Bwo", 
                ReleaseYear = 2003,
                Description = "Gandalf and Aragorn lead the World of Men against Sauron's army to draw his gaze from Frodo and Sam as they approach Mount Doom with the One Ring.",
                FranchiseId = 1
            },
            
            // The Hobbit trilogy
            new()
            {
                Id = 4, 
                Director = "Peter Jackson", 
                Genre = "Fantasy, Adventure", 
                Picture = "https://upload.wikimedia.org/wikipedia/en/b/b3/The_Hobbit-_An_Unexpected_Journey.jpeg", 
                Title = "The Hobbit: An Unexpected Journey", 
                Trailer = "https://www.youtube.com/watch?v=9PSXjr1gbjc", 
                ReleaseYear = 2012,
                Description = "A reluctant Hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home, and the gold within it from the dragon Smaug.",
                FranchiseId = 1
            },
            new()
            {
                Id = 5, 
                Director = "Peter Jackson", 
                Genre = "Fantasy, Adventure", 
                Picture = "https://upload.wikimedia.org/wikipedia/en/4/4f/The_Hobbit_-_The_Desolation_of_Smaug_theatrical_poster.jpg", 
                Title = "The Hobbit: The Desolation of Smaug", 
                Trailer = "https://www.youtube.com/watch?v=fnaojlfdUbs", 
                ReleaseYear = 2013,
                Description = "The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring.",
                FranchiseId = 1
            },
            new()
            {
                Id = 6, 
                Director = "Peter Jackson", 
                Genre = "Fantasy, Adventure", 
                Picture = "https://upload.wikimedia.org/wikipedia/en/0/0e/The_Hobbit_-_The_Battle_of_the_Five_Armies.jpg", 
                Title = "The Hobbit: The Battle of the Five Armies", 
                Trailer = "https://www.youtube.com/watch?v=ZSzeFFsKEt4", 
                ReleaseYear = 2014,
                Description = "Bilbo and company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness.",
                FranchiseId = 1
            },
            
            // Harry Potter saga
            new()
            {
                Id = 7, 
                Director = "Chris Columbus", 
                Genre = "Fantasy, Adventure, Family", 
                Picture = "https://upload.wikimedia.org/wikipedia/en/7/7a/Harry_Potter_and_the_Philosopher%27s_Stone_banner.jpg",
                Title = "Harry Potter and the Philosopher's Stone", 
                Trailer = "https://www.youtube.com/watch?v=VyHV0BRtdxo", 
                ReleaseYear = 2001,
                Description = "An orphaned boy enrolls in a school of wizardry, where he learns the truth about himself, his family and the terrible evil that haunts the magical world.",
                FranchiseId = 2
            },
            new()
            {
                Id = 8, 
                Director = "Chris Columbus", 
                Genre = "Fantasy, Adventure, Family",
                Picture = "https://upload.wikimedia.org/wikipedia/en/c/c0/Harry_Potter_and_the_Chamber_of_Secrets_movie.jpg",
                Title = "Harry Potter and the Chamber of Secrets", 
                Trailer = "https://www.youtube.com/watch?v=1bq0qff4iF8", 
                ReleaseYear = 2002,
                Description = "An ancient prophecy seems to be coming true when a mysterious presence begins stalking the corridors of a school of magic and leaving its victims paralyzed.",
                FranchiseId = 2
            },
            new()
            {
                Id = 9, 
                Director = "Alfonso Cuarón", 
                Genre = "Fantasy, Adventure, Family", 
                Picture = "https://upload.wikimedia.org/wikipedia/en/b/bc/Prisoner_of_azkaban_UK_poster.jpg", 
                Title = "Harry Potter and the Prisoner of Azkaban", 
                Trailer = "https://www.youtube.com/watch?v=1ZdlAg3j8nI",
                ReleaseYear = 2004,
                Description = "Harry Potter, Ron and Hermione return to Hogwarts School of Witchcraft and Wizardry for their third year of study, where they delve into the mystery surrounding an escaped prisoner who poses a dangerous threat to the young wizard.",
                FranchiseId = 2
            },
            new()
            {
                Id = 10, 
                Director = "Mike Newell", 
                Genre = "Fantasy, Adventure, Family",
                Picture = "https://upload.wikimedia.org/wikipedia/en/c/c9/Harry_Potter_and_the_Goblet_of_Fire_Poster.jpg", 
                Title = "Harry Potter and the Goblet of Fire", 
                Trailer = "https://www.youtube.com/watch?v=3EGojp4Hh6I", 
                ReleaseYear = 2005,
                Description = "Harry Potter finds himself competing in a hazardous tournament between rival schools of magic, but he is distracted by recurring nightmares.",
                FranchiseId = 2
            },
            new()
            {
                Id = 11, 
                Director = "David Yates",
                Genre = "Fantasy, Adventure, Family, Action", 
                Picture = "https://upload.wikimedia.org/wikipedia/en/e/e7/Harry_Potter_and_the_Order_of_the_Phoenix_poster.jpg", 
                Title = "Harry Potter and the Order of the Phoenix", 
                Trailer = "https://www.youtube.com/watch?v=y6ZW7KXaXYk", 
                ReleaseYear = 2007,
                Description = "With their warning about Lord Voldemort's return scoffed at, Harry and Dumbledore are targeted by the Wizard authorities as an authoritarian bureaucrat slowly seizes power at Hogwarts.",
                FranchiseId = 2
            },
            new()
            {
                Id = 12, 
                Director = "David Yates", 
                Genre = "Fantasy, Adventure, Family, Action", 
                Picture = "https://upload.wikimedia.org/wikipedia/en/3/3f/Harry_Potter_and_the_Half-Blood_Prince_poster.jpg", 
                Title = "Harry Potter and the Half-Blood Prince", 
                Trailer = "https://www.youtube.com/watch?v=sg81Lts5kYY", 
                ReleaseYear = 2009,
                Description = "As Harry Potter begins his sixth year at Hogwarts, he discovers an old book marked as 'the property of the Half-Blood Prince' and begins to learn more about Lord Voldemort's dark past.",
                FranchiseId = 2
            },
            new()
            {
                Id = 13, 
                Director = "David Yates", 
                Genre = "Fantasy, Adventure, Family", 
                Picture = "https://upload.wikimedia.org/wikipedia/en/2/2d/Harry_Potter_and_the_Deathly_Hallows_–_Part_1.jpg", 
                Title = "Harry Potter and the Deathly Hallows: Part 1", 
                Trailer = "https://www.youtube.com/watch?v=MxqsmsA8y5k", 
                ReleaseYear = 2011,
                Description = "As Harry, Ron, and Hermione race against time and evil to destroy the Horcruxes, they uncover the existence of the three most powerful objects in the wizarding world: the Deathly Hallows.",
                FranchiseId = 2
            },
            new()
            {
                Id = 14, 
                Director = "David Yates", 
                Genre = "Fantasy, Adventure, Family, Mystery", 
                Picture = "https://upload.wikimedia.org/wikipedia/en/d/df/Harry_Potter_and_the_Deathly_Hallows_–_Part_2.jpg", 
                Title = "Harry Potter and the Deathly Hallows: Part 2", 
                Trailer = "https://www.youtube.com/watch?v=PUXpgitNeOU", 
                ReleaseYear = 2012,
                Description = "Harry, Ron, and Hermione search for Voldemort's remaining Horcruxes in their effort to destroy the Dark Lord as the final battle rages on at Hogwarts.",
                FranchiseId = 2
            },
            
            // Fantastic Beasts series
            new()
            {
                Id = 15, 
                Director = "David Yates", 
                Genre = "Fantasy, Adventure, Family", 
                Picture = "https://upload.wikimedia.org/wikipedia/en/5/5e/Fantastic_Beasts_and_Where_to_Find_Them_poster.png", 
                Title = "Fantastic Beasts and Where to Find Them", 
                Trailer = "https://www.youtube.com/watch?v=ViuDsy7yb8M", 
                ReleaseYear = 2016,
                Description = "The adventures of writer Newt Scamander in New York's secret community of witches and wizards seventy years before Harry Potter reads his book in school.",
                FranchiseId = 2
            },
            new()
            {
                Id = 16, 
                Director = "David Yates", 
                Genre = "Fantasy, Adventure, Family",
                Picture = "https://upload.wikimedia.org/wikipedia/en/3/3c/Fantastic_Beasts_-_The_Crimes_of_Grindelwald_Poster.png",
                Title = "Fantastic Beasts: The Crimes of Grindelwald",
                Trailer = "https://www.youtube.com/watch?v=5sEaYB4rLFQ",
                ReleaseYear = 2018,
                Description = "The second installment of the 'Fantastic Beasts' series featuring the adventures of Magizoologist Newt Scamander.",
                FranchiseId = 2
            },
            new()
            {
                Id = 17, 
                Director = "David Yates", 
                Genre = "Fantasy, Action, Adventure", 
                Picture = "https://upload.wikimedia.org/wikipedia/en/3/34/Fantastic_Beasts-_The_Secrets_of_Dumbledore.png", 
                Title = "Fantastic Beasts: The Secrets of Dumbledore", 
                Trailer = "https://www.youtube.com/watch?v=Y9dr2zw-TXQ",
                ReleaseYear = 2022,
                Description = "Albus Dumbledore assigns Newt and his allies with a mission related to the rising power of Grindelwald.",
                FranchiseId = 2
            }
        };
    }
    
    /// <summary>
    /// Creates a list of characters to initialize database
    /// </summary>
    /// <returns>List of characters</returns>
    public static IEnumerable<Character> GetCharacters()
    {
        return new List<Character>()
        {
            // Middle-earth (The Lord of the rings and The Hobbit trilogies) characters
            new()
            {
                Id = 1,
                FullName = "Bilbo Baggins",
                Alias = "Mr. Baggins",
                Gender = "Male",
                Picture = "https://upload.wikimedia.org/wikipedia/en/2/2f/Young_Bilbo.jpg"
            },
            new()
            {
                Id = 2,
                FullName = "Frodo Baggins",
                Alias = "Frodo of the Nine Fingers",
                Gender = "Male",
                Picture = "https://upload.wikimedia.org/wikipedia/en/4/4e/Elijah_Wood_as_Frodo_Baggins.png"
            },
            new()
            {
                Id = 3,
                FullName = "Gandalf",
                Alias = "Mithrandir",
                Gender = "Male",
                Picture = "https://upload.wikimedia.org/wikipedia/en/e/e9/Gandalf600ppx.jpg"
            },

            // The Lord of the Rings characters
            new()
            {
                Id = 4,
                FullName = "Aragorn II Elessar",
                Alias = "Strider",
                Gender = "Male",
                Picture = "https://upload.wikimedia.org/wikipedia/en/3/35/Aragorn300ppx.png"
            },
            new()
            {
                Id = 5,
                FullName = "Galadriel",
                Alias = "Lady of Light",
                Gender = "Female",
                Picture = "https://upload.wikimedia.org/wikipedia/en/c/cb/Galadriel.jpg"
            },

            // The Hobbit trilogy
            new()
            {
                Id = 6,
                FullName = "Smaug",
                Alias = "Smaug the Terrible",
                Gender = "Male",
                Picture = "https://upload.wikimedia.org/wikipedia/en/f/f2/Smaug_%28Peter_Jackson_Hobbit_Trilogy%29.jpg"
            },
            new()
            {
                Id = 7,
                FullName = "Thorin II 'Oakenshield'",
                Alias = "King of Durin's Folk",
                Gender = "Male",
                Picture = "https://upload.wikimedia.org/wikipedia/en/e/ed/Thorin%2C_from_the_Hobbit.jpg"
            },

            // Wizarding World (Harry Potter and Fantastic Beasts) characters
            new()
            {
                Id = 8,
                FullName = "Albus Percival Wulfric Brian Dumbledore",
                Alias = "Dumbledore",
                Gender = "Male",
                Picture = "https://upload.wikimedia.org/wikipedia/en/f/fe/Dumbledore_and_Elder_Wand.JPG"
            },

            // Harry Potter saga characters
            new()
            {
                Id = 9,
                FullName = "Harry James Potter",
                Alias = "The boy who lived",
                Gender = "Male",
                Picture = "https://upload.wikimedia.org/wikipedia/en/d/d7/Harry_Potter_character_poster.jpg"
            },

            // Fantastic Beasts series characters
            new()
            {
                Id = 10,
                FullName = "Newton Artemis Fido Scamander",
                Alias = "Newt",
                Gender = "Male",
                Picture =
                    "https://static.wikia.nocookie.net/harrypotter/images/3/36/Newton_Scamander_Profile_crop.png/revision/latest?cb=20190609204955"
            }
        };
    }

    /// <summary>
    /// Creates a list of franchises to initialize database
    /// </summary>
    /// <returns>List of franchises</returns>
    public static IEnumerable<Franchise> GetFranchises()
    {
        return new List<Franchise>()
        {
            new()
            {
                Id = 1,
                Name = "Middle-earth",
                Description = "Middle-earth is the fictional setting of much of the English writer J. R. R. Tolkien's fantasy. New Line Cinema released the first part of director Peter Jackson's The Lord of the Rings film series in 2001 as part of a trilogy and several actors and roles were introduced once again in a trilogy in The Hobbit film series. In 2017, Amazon co-operated with New Line to acquire the television rights to adapt a new prequel show set in a period glimpsed during a flashback in The Lord of the Rings films."
            },
            new()
            {
                Id = 2,
                Name = "Wizarding World",
                Description = "The Wizarding World (previously known as J. K. Rowling's Wizarding World) is a fantasy media franchise and shared fictional universe centred on the Harry Potter novel series by J. K. Rowling. A series of films have been in production since 2000, and in that time eleven films have been produced—eight are adaptations of the Harry Potter novels and three are part of the Fantastic Beasts series."
            }
        };
    }
}