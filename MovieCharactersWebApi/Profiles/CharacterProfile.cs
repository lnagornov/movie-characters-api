using AutoMapper;
using MovieCharactersWebApi.Models.Domain;
using MovieCharactersWebApi.Models.DTO.CharacterDTO;
using MovieCharactersWebApi.Models.DTO.MovieDTO;

namespace MovieCharactersWebApi.Profiles;

public class CharacterProfile : Profile
{
    public CharacterProfile()
    {
        CreateMap<Character, CharacterReadDto>()
            .ForMember(dto => dto.Movies, opt => opt
            .MapFrom(character => character.Movies
            .Select(movie => new MovieWithOutCharactersReadDto
            {
                Id = movie.Id,
                Description = movie.Description,
                Director = movie.Director,
                FranchiseId = movie.FranchiseId,
                Genre = movie.Genre,
                Picture = movie.Picture,
                ReleaseYear = movie.ReleaseYear,
                Title = movie.Title,
                Trailer = movie.Trailer
            })
            .ToList()));
        
        CreateMap<CharacterCreateDto, Character>();
        CreateMap<CharacterEditDto, Character>();
    }
}