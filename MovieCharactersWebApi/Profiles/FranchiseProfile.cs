using AutoMapper;
using MovieCharactersWebApi.Models.Domain;
using MovieCharactersWebApi.Models.DTO.FranchiseDTO;

namespace MovieCharactersWebApi.Profiles;

public class FranchiseProfile : Profile
{
    public FranchiseProfile()
    {
        CreateMap<Franchise, FranchiseReadDto>()
            .ForMember(dto => dto.Movies, opt => opt
            .MapFrom(character => character.Movies
            .Select(movie => movie.Id)
            .ToList()));
        
        CreateMap<FranchiseCreateDto, Franchise>();
        CreateMap<FranchiseEditDto, Franchise>();
    }
}