using AutoMapper;
using MovieCharactersWebApi.Models.Domain;
using MovieCharactersWebApi.Models.DTO.CharacterDTO;
using MovieCharactersWebApi.Models.DTO.MovieDTO;

namespace MovieCharactersWebApi.Profiles;

public class MovieProfile : Profile
{
    public MovieProfile()
    {
        CreateMap<Movie, MovieReadDto>()
            .ForMember(dto => dto.Characters, opt => opt
            .MapFrom(movie => movie.Characters
            .Select(character => new CharacterWithOutMoviesReadDto
            {
                Id = character.Id,
                Alias = character.Alias,
                FullName = character.FullName,
                Gender = character.Gender,
                Picture = character.Picture
            })
            .ToList()));

        CreateMap<MovieCreateDto, Movie>();
        CreateMap<MovieEditDto, Movie>();
    }
}