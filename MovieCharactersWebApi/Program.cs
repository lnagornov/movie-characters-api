using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using MovieCharactersWebApi.Models;
using MovieCharactersWebApi.Repositories;
using MovieCharactersWebApi.Repositories.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();

// Configure swagger
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
        {
            Title = "Movie Characters RESTful API",
            Version = "v1",
            Description =
                "RESTful ASP.NET Core Web API to manage movies, characters and franchises. Made for Noroff .Net Course Assignment #6.",
            TermsOfService = new Uri("https://example.com/terms"),
            Contact = new OpenApiContact
            {
                Name = "Lev Nagornov",
                Email = "LNagornov@yahoo.com",
                Url = new Uri("https://www.linkedin.com/in/lnagornov/")
            },
            License = new OpenApiLicense
            {
                Name = "MIT",
                Url = new Uri("https://opensource.org/licenses/MIT")
            }
        }
    );
    // Set the comments path for the Swagger JSON and UI.
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    options.IncludeXmlComments(xmlPath);
});

// Add db context
builder.Services.AddDbContext<MovieCharactersDbContext>(options =>
    options.UseNpgsql(builder.Configuration.GetConnectionString("PostgresLocalDbConnection")));

// Add Repositories to access db
builder.Services.AddScoped(typeof(IMovieAsyncRepository), typeof(MovieAsyncRepository));
builder.Services.AddScoped(typeof(ICharacterAsyncRepository), typeof(CharacterAsyncRepository));
builder.Services.AddScoped(typeof(IFranchiseAsyncRepository), typeof(FranchiseAsyncRepository));

// Add AutoMapper to map model and DTO
builder.Services.AddAutoMapper(typeof(Program));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();