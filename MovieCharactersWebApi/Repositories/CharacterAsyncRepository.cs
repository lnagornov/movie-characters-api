using Microsoft.EntityFrameworkCore;
using MovieCharactersWebApi.Models;
using MovieCharactersWebApi.Models.Domain;
using MovieCharactersWebApi.Repositories.Interfaces;

namespace MovieCharactersWebApi.Repositories;

public class CharacterAsyncRepository : ICharacterAsyncRepository
{
    private readonly MovieCharactersDbContext _context;

    public CharacterAsyncRepository(MovieCharactersDbContext context)
    {
        _context = context;
    }
    
    /// <summary>
    /// Get a character by character id from the database asynchronously
    /// </summary>
    /// <param name="id">Id of the character to get</param>
    /// <returns>Task with a result as a character</returns>
    public async Task<Character?> GetByIdAsync(int id)
    {
        return await _context.Character
            .Include(character => character.Movies)
            .FirstOrDefaultAsync(character => character.Id == id);
    }

    /// <summary>
    /// Get all characters from the database asynchronously
    /// </summary>
    /// <returns>Task with a result as a list of characters</returns>
    public async Task<IEnumerable<Character?>> GetAllAsync()
    {
        return await _context.Character
            .Include(character => character.Movies)
            .ToListAsync();
    }

    /// <summary>
    /// Checks if the given character id exists in the database asynchronously
    /// </summary>
    /// <param name="id">Character id to check</param>
    /// <returns>Task with a result as boolean</returns>
    public async Task<bool> ExistsWithIdAsync(int id)
    {
        return await _context.Character
            .AnyAsync(character => character.Id == id);
    }

    /// <summary>
    /// Add a character to the database asynchronously
    /// </summary>
    /// <param name="character">Character to add</param>
    /// <returns>Task with a result as an added character</returns>
    public async Task<Character?> AddAsync(Character character)
    {
        _context.Character.Add(character);
        await _context.SaveChangesAsync();

        return character;
    }

    /// <summary>
    /// Update a character in the database asynchronously
    /// </summary>
    /// <param name="character">Character with updated fields</param>
    public async Task UpdateAsync(Character character)
    {
        _context.Entry(character).State = EntityState.Modified;
        await _context.SaveChangesAsync();
    }

    /// <summary>
    /// Delete a character from the database asynchronously
    /// </summary>
    /// <param name="id">Character id to delete character</param>
    public async Task DeleteByIdAsync(int id)
    {
        var character = await _context.Character.FindAsync(id);
        _context.Character.Remove(character!);
        await _context.SaveChangesAsync();
    }
}