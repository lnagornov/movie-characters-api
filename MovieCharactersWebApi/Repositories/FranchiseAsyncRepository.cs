using Microsoft.EntityFrameworkCore;
using MovieCharactersWebApi.Models;
using MovieCharactersWebApi.Models.Domain;
using MovieCharactersWebApi.Repositories.Interfaces;

namespace MovieCharactersWebApi.Repositories;

public class FranchiseAsyncRepository : IFranchiseAsyncRepository
{
    private readonly MovieCharactersDbContext _context;

    public FranchiseAsyncRepository(MovieCharactersDbContext context)
    {
        _context = context;
    }
    
    /// <summary>
    /// Get a franchise by franchise id from the database asynchronously
    /// </summary>
    /// <param name="id">Id of the franchise to get</param>
    /// <returns>Task with a result as a franchise</returns>
    public async Task<Franchise?> GetByIdAsync(int id)
    {
        return await _context.Franchise
            .Include(franchise => franchise.Movies)
            .FirstOrDefaultAsync(franchise => franchise.Id == id);
    }

    /// <summary>
    /// Get all franchises from the database asynchronously
    /// </summary>
    /// <returns>Task with a result as a list of franchises</returns>
    public async Task<IEnumerable<Franchise?>> GetAllAsync()
    {
        return await _context.Franchise
            .Include(franchise => franchise.Movies)
            .ToListAsync();
    }

    /// <summary>
    /// Checks if the given franchise id exists in the database asynchronously
    /// </summary>
    /// <param name="id">Franchise id to check</param>
    /// <returns>Task with a result as boolean</returns>
    public async Task<bool> ExistsWithIdAsync(int id)
    {
        return await _context.Franchise.AnyAsync(franchise => franchise.Id == id);
    }

    /// <summary>
    /// Add a franchise to the database asynchronously
    /// </summary>
    /// <param name="franchise">franchise to add</param>
    /// <returns>Task with a result as an added franchise</returns>
    public async Task<Franchise?> AddAsync(Franchise franchise)
    {
        _context.Franchise.Add(franchise);
        await _context.SaveChangesAsync();

        return franchise;
    }

    /// <summary>
    /// Update a franchise in the database asynchronously
    /// </summary>
    /// <param name="franchise">Franchise with updated fields</param>
    public async Task UpdateAsync(Franchise franchise)
    {
        _context.Entry(franchise).State = EntityState.Modified;
        await _context.SaveChangesAsync();
    }

    /// <summary>
    /// Delete a franchise from the database asynchronously
    /// </summary>
    /// <param name="id">Franchise id to delete franchise</param>
    public async Task DeleteByIdAsync(int id)
    {
        var franchise = await _context.Franchise.FindAsync(id);
        _context.Franchise.Remove(franchise!);
        await _context.SaveChangesAsync();
    }

    /// <summary>
    /// Get all movies matching one franchise by franchise id from the database asynchronously
    /// </summary>
    /// <param name="id">Franchise id to get movies</param>
    /// <returns>Task with result as a list of movies</returns>
    public async Task<IEnumerable<Movie?>> GetMoviesByFranchiseIdAsync(int id)
    {
        return await _context.Movie
            .Include(movie => movie.Characters)
            .Where(movie => movie.FranchiseId == id)
            .ToListAsync();
    }

    /// <summary>
    /// Get all characters matching one franchise by franchise id from the database asynchronously
    /// </summary>
    /// <param name="id">Franchise id to get characters</param>
    /// <returns>Task with result as a list of characters</returns>
    public async Task<IEnumerable<Character?>> GetCharactersByFranchiseIdAsync(int id)
    {
        return await _context.Character
            .Include(character => character.Movies)
            .Where(character => character.Movies.Any(movie => movie.FranchiseId == id))
            .ToListAsync();
    }

    /// <summary>
    /// Update all movies in a specific franchise with given enumerable of movie ids asynchronously
    /// </summary>
    /// <param name="id">Franchise id to update characters</param>
    /// <param name="movieIds">Enumerable of movie ids to replace</param>
    public async Task UpdateFranchiseMoviesByFranchiseIdAsync(int id, IEnumerable<int> movieIds)
    {
        var franchiseToUpdate = await GetByIdAsync(id);
        var newMovies = new List<Movie?>();
        foreach (var movieId in movieIds)
        {
            var movie = await _context.Movie!.FindAsync(movieId);
            if (movie is null)
            {
                throw new KeyNotFoundException($"Movie with id {movieId} not found!");
            }

            newMovies.Add(movie);
        }

        franchiseToUpdate!.Movies = newMovies;
        await UpdateAsync(franchiseToUpdate);
    }
}