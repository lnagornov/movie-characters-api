using MovieCharactersWebApi.Models.Domain;

namespace MovieCharactersWebApi.Repositories.Interfaces;

public interface ICharacterAsyncRepository : IAsyncRepository<Character>
{
}