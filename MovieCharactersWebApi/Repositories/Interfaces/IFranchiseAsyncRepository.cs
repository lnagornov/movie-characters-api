using MovieCharactersWebApi.Models.Domain;

namespace MovieCharactersWebApi.Repositories.Interfaces;

public interface IFranchiseAsyncRepository : IAsyncRepository<Franchise>
{
    /// <summary>
    /// Get all movies matching one franchise by franchise id from the database asynchronously
    /// </summary>
    /// <param name="id">Franchise id to get movies</param>
    /// <returns>Task with result as a list of movies</returns>
    Task<IEnumerable<Movie?>> GetMoviesByFranchiseIdAsync(int id);
    
    /// <summary>
    /// Get all characters matching one franchise by franchise id from the database asynchronously
    /// </summary>
    /// <param name="id">Franchise id to get characters</param>
    /// <returns>Task with result as a list of characters</returns>
    Task<IEnumerable<Character?>> GetCharactersByFranchiseIdAsync(int id);
    
    /// <summary>
    /// Update all movies in a specific franchise with given enumerable of movie ids asynchronously
    /// </summary>
    /// <param name="id">Franchise id to update characters</param>
    /// <param name="movieIds">Enumerable of movie ids to replace</param>
    Task UpdateFranchiseMoviesByFranchiseIdAsync(int id, IEnumerable<int> movieIds);
}