using MovieCharactersWebApi.Models.Domain;

namespace MovieCharactersWebApi.Repositories.Interfaces;

public interface IMovieAsyncRepository : IAsyncRepository<Movie>
{
    /// <summary>
    /// Get all characters matching one movie by movie id from the database asynchronously
    /// </summary>
    /// <param name="id">Movie id to get characters</param>
    /// <returns>Task with result as a list of characters</returns>
    Task<IEnumerable<Character?>> GetCharactersByMovieIdAsync(int id);

    /// <summary>
    /// Update all characters in a specific movie with given enumerable of character ids asynchronously
    /// </summary>
    /// <param name="id">Movie id to update characters</param>
    /// <param name="characterIds">Enumerable of character ids to replace</param>
    Task UpdateMovieCharactersByMovieIdAsync(int id, IEnumerable<int> characterIds);
}