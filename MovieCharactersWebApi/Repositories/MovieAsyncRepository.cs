using Microsoft.EntityFrameworkCore;
using MovieCharactersWebApi.Models;
using MovieCharactersWebApi.Models.Domain;
using MovieCharactersWebApi.Repositories.Interfaces;

namespace MovieCharactersWebApi.Repositories;

public class MovieAsyncRepository : IMovieAsyncRepository
{
    private readonly MovieCharactersDbContext _context;

    public MovieAsyncRepository(MovieCharactersDbContext context)
    {
        _context = context;
    }

    /// <summary>
    /// Get a movie by movie id from the database asynchronously
    /// </summary>
    /// <param name="id">Id of the movie to get</param>
    /// <returns>Task with a result as a movie</returns>
    public async Task<Movie?> GetByIdAsync(int id)
    {
        return await _context.Movie
            .Include(movie => movie.Characters)
            .FirstOrDefaultAsync(movie => movie.Id == id);
    }

    /// <summary>
    /// Get all movies from the database asynchronously
    /// </summary>
    /// <returns>Task with a result as a list of movies</returns>
    public async Task<IEnumerable<Movie?>> GetAllAsync()
    {
        return await _context.Movie
            .Include(movie => movie.Characters)
            .ToListAsync();
    }
    
    /// <summary>
    /// Checks if the given movie id exists in the database asynchronously
    /// </summary>
    /// <param name="id">Movie id to check</param>
    /// <returns>Task with a result as boolean</returns>
    public async Task<bool> ExistsWithIdAsync(int id)
    {
        return await _context.Movie.AnyAsync(movie => movie.Id == id);
    }

    /// <summary>
    /// Add a movie to the database asynchronously
    /// </summary>
    /// <param name="movie">Movie to add</param>
    /// <returns>Task with a result as an added movie</returns>
    public async Task<Movie?> AddAsync(Movie movie)
    {
        _context.Movie.Add(movie);
        await _context.SaveChangesAsync();

        return movie;
    }

    /// <summary>
    /// Update a movie in the database asynchronously
    /// </summary>
    /// <param name="movie">Movie with updated fields</param>
    public async Task UpdateAsync(Movie movie)
    {
        _context.Entry(movie).State = EntityState.Modified;
        await _context.SaveChangesAsync();
    }

    /// <summary>
    /// Delete a movie from the database asynchronously
    /// </summary>
    /// <param name="id">Movie id to delete movie</param>
    public async Task DeleteByIdAsync(int id)
    {
        var movie = await _context.Movie.FindAsync(id);
        _context.Movie.Remove(movie!);
        await _context.SaveChangesAsync();
    }
    
    /// <summary>
    /// Get all characters matching one movie by movie id from the database asynchronously
    /// </summary>
    /// <param name="id">Movie id to get characters</param>
    /// <returns>Task with result as a list of characters</returns>
    public async Task<IEnumerable<Character?>> GetCharactersByMovieIdAsync(int id)
    {
        return await _context.Character
            .Include(character => character.Movies)
            .Where(character => character.Movies
                .Any(movie => movie.Id == id))
            .ToListAsync();
    }

    /// <summary>
    /// Update all characters in a specific movie with given array of character ids asynchronously
    /// </summary>
    /// <param name="id">Movie id to update characters</param>
    /// <param name="characterIds">Array of character ids to replace</param>
    /// <exception cref="KeyNotFoundException">One of the character ids to replace does not exist</exception>
    public async Task UpdateMovieCharactersByMovieIdAsync(int id, IEnumerable<int> characterIds)
    {
        var movieToUpdate = await GetByIdAsync(id);
        var newCharacters = new List<Character?>();
        foreach (var characterId in characterIds)
        {
            var character = await _context.Character!.FindAsync(characterId);
            if (character is null)
            {
                throw new KeyNotFoundException($"Character with id {characterId} not found!");
            }

            newCharacters.Add(character);
        }

        movieToUpdate!.Characters = newCharacters;
        await UpdateAsync(movieToUpdate);
    }
}